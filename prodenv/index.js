const express = require('express');
const fs = require('fs');
const path = require('path');
const request = require('request');
const app = express();

app.disable('x-powered-by');

port = process.env.PORT || process.argv[2] || 4200;

app.use('/', express.static('public'));

app.all('/*', function(req, res, next) {
  res.sendFile('./public/index.html', { root: __dirname });
});

app.listen(port, function() {
  console.log('app up on port: ' + port);
});
