export interface CreateDoc {
    id: string;
    execer: string;
    messageType: string;
    documentId: string;
    documentCreator: string;
    documentName: string;
    documentDescription: string;
    documentCompany ?: string;
    documentDept: string;
    documentTyp: string;
    documentBlob: string;
    documentTags ?: string;
    documentVerifier ?: string;
    documentSigners ?: string;
    documentMasks ?: string;
    documentParent ?: string; 
  
}