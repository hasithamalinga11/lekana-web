export interface CreateUser {
    id: string, 
    execer: string,
    messageType: string,
    accountId: string,
    accountPassword ?: string,
    accountName : string,
    accountPhone: string,
    accountEmail: string,
    accountCompany?: string,
    deviceToken?: "",
    deviceType?: "",
    accountRoles?: ""
}