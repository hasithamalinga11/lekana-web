import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

declare var moment;
@Pipe({ name: 'dateFormatPipe' })
export class DateFormatPipe implements PipeTransform {
  transform(v: string, arg: string): string {
    if (arg == 'dmy-event') {
      return moment().format('YYYY MMMM DD');
    }
    if (arg == 'now-date') {
      return moment().format('MMMM DD');
    }
    if (arg == 'now-time') {
      return moment().format('LT');
    }
    if (arg == 'now') {
      return moment().format('YYYY MMMM DD LT');
    }
    let str = moment.unix(+v).format('YYYY MMM DD LT');
    if (arg == 'ymd') {
      str = moment.unix(+v).format('YYYY-MM-DD');
    }

    return str;
  }
}
export const transformDateTime = (value: string, format: string, toFormat: string): string => {
  value = moment(value, format).format(toFormat);
  return value;
};
@Pipe({ name: 'safeUrlpdf' })
export class SafeUrlC {
  constructor(private sanitizer: DomSanitizer) {}

  transform(style) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(style);
    //return this.sanitizer.bypassSecurityTrustStyle(style);
    // return this.sanitizer.bypassSecurityTrustXxx(style); - see docs
  }
}
