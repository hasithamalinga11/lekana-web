import { Pipe, PipeTransform } from '@angular/core';
import { formatMatricNumber } from 'src/app/utils/Utils';
import { CURRENCT_PREFIX } from '../constants/app.constants';

@Pipe({ name: 'pricePipe' })
export class PricePipe implements PipeTransform {
  transform(v: string, arg: string): string {
    const value: string = '' + parseInt(v, 10);
    const vx = parseInt(value, 10);
    let str = '';
    if (arg != 'numberOnly') {
      str = CURRENCT_PREFIX + ' ' + vx.toLocaleString().split('.')[0];
    }
    if (arg == 'numberOnly') {
      str = vx.toLocaleString();
    }
    if (arg == 'M') {
      str = CURRENCT_PREFIX + ' ' + formatMatricNumber(vx);
    }
    if (arg == 'M_number') {
      str = formatMatricNumber(vx);
    }
    if (arg == 'numberOnlyINT') {
      str = vx.toLocaleString().split('.')[0];
    }
    return str;
  }
}
