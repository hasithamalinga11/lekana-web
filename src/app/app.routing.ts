import { HomePageComponent } from "./pages/home-page/home-page.component";
import { Routes } from "@angular/router";
import { PageNotFoundComponent } from "./pages/page-not-found/page-not-found.component";
import { LoginPageComponent } from "./pages/login-page/login-page.component";
import { SignupPageComponent } from "./pages/signup-page/signup-page.component";
import { DashboardPageComponent } from "./pages/account/dashboard-page/dashboard-page.component";
import { StartNewPageComponent } from "./pages/account/start-new-page/start-new-page.component";
import { StartNowFormPageComponent } from "./pages/account/start-now-form-page/start-now-form-page.component";
import { ApplicationsPageComponent } from "./pages/applications-page/applications-page.component";
import { UserAuthGuard } from "./services/auth.guard";
import { UserPageComponent } from "./pages/user-page/user-page.component";
import { NewUserComponent } from "./pages/user-page/new-user/new-user.component";
import { LandingComponent } from "./pages/landing/landing.component";
import { UserViewComponent } from "./pages/user-page/user-view/user-view.component";
import { UserEditComponent } from "./pages/user-page/user-edit/user-edit.component";
import { DocViewComponent } from "./pages/applications-page/doc-view/doc-view.component";
import { SettingComponents } from "./pages/setting/setting.component";
export const appRoutes: Routes = [
  { path: "", component: LandingComponent },
  { path: "login", component: LoginPageComponent },
  { path: "signup", component: SignupPageComponent },
  {
    path: "app",
    component: DashboardPageComponent,
    canActivate: [UserAuthGuard],
    children: [
      { path: "", redirectTo: "users", pathMatch: "full" },
      { path: "application", component: StartNowFormPageComponent },
      { path: "applications", component: ApplicationsPageComponent },
      { path: "application/view", component: DocViewComponent },
      { path: "users", component: UserPageComponent },
      { path: "users/new-user", component: NewUserComponent },
      { path: "user", component: UserViewComponent },
      { path: "user/edit", component: UserEditComponent },
      { path: "setting", component: SettingComponents }
    ]
  },
  { path: "**", component: PageNotFoundComponent }
];
