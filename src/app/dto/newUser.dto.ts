export class NewUser {
  activated?: boolean;
  company?: string;
  disabled?: boolean;
  email?: string;
  id: string;
  name: string;
  phone: string;
  roles: string;
  timestamp?: string;
}
