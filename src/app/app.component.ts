import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { IUser } from './models/IUser';
import { AuthService } from './services/auth.service';
import { AppContextService } from './services/app-context.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  userAccount: IUser = null;

  constructor(
    public appContextService: AppContextService,
    private authService: AuthService,
    private router: Router,
    private ch: ChangeDetectorRef
  ) {
    this.authService.userAccount.subscribe(user => {
      this.userAccount = user;
      window.scrollTo(0, 0);
    });
    this.initApp();
  }
  async initApp() {
    // const athResult = await this.authService.authCheck();
    // if (athResult) {
    //   this.authService.authState.next(UserAuthState.userLoggedIn);
    // } else {
    //   this.authService.authState.next(UserAuthState.guest);
    // }
  }
  async ngOnInit() {
    await this.authService.authCheck();
  }
  ngAfterViewInit(): void {
    window.scrollTo(0, 0);
  }
  logout() {
    this.authService.signout();
    location.href = './';
  }
}

//Side nav helper functions:
// @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;
// toggleSideNav(tag: string = '') {
//   if (tag == 'hide') {
//     this.sidenav.toggle(false);
//   } else {
//     this.sidenav.toggle();
//   }
// }

// if (window.innerWidth < 900) {
//   this.sidenav.toggle(false);
//   this.ch.detectChanges();
// }
