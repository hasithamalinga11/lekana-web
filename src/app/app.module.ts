import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppComponent } from "./app.component";
import { CommonModule } from "@angular/common";
import { MaterialLibsModule } from "./libs/material-libs.module";
import { HttpClientModule } from "@angular/common/http";
import { PageNotFoundComponent } from "./pages/page-not-found/page-not-found.component";
import { RouterModule } from "@angular/router";

import { TruncatePipe } from "./pipes/truncate";

import { appRoutes } from "./app.routing";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { CommonUiModule } from "./libs/common-ui.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppCommonService } from "./services/app-common.service";
import { LoginPageComponent } from "./pages/login-page/login-page.component";
import { SignupPageComponent } from "./pages/signup-page/signup-page.component";
import { DashboardPageComponent } from "./pages/account/dashboard-page/dashboard-page.component";
import { StartNewPageComponent } from "./pages/account/start-new-page/start-new-page.component";
import { StartNowFormPageComponent } from "./pages/account/start-now-form-page/start-now-form-page.component";
import { ApplicationsPageComponent } from "./pages/applications-page/applications-page.component";
import { UserService } from "./services/user.service";
import { SafeUrlC } from "./pipes/date-format.pipe";
import { AuthService } from "./services/auth.service";
import { AppContextService } from "./services/app-context.service";
import { UserPageComponent } from "./pages/user-page/user-page.component";
import { UserPageService } from "./services/usersPage.service";
import { UserComponent } from "./pages/user-page/user/user.component";
import { NewUserComponent } from "./pages/user-page/new-user/new-user.component";
import { DocCardComponent } from "./pages/applications-page/doc-card/doc-card.component";
import { DragDropDirective } from "./directives/dragDrop.directive";
import { LandingComponent } from "./pages/landing/landing.component";
import { UserViewComponent } from "./pages/user-page/user-view/user-view.component";
import { DocViewComponent } from "./pages/applications-page/doc-view/doc-view.component";
import { SettingComponents } from "./pages/setting/setting.component";
import { NgPdfPageSignComponent } from "./components/ng-pdf-page-sign/ng-pdf-page-sign.component";
import { UserEditComponent } from "./pages/user-page/user-edit/user-edit.component";
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    TruncatePipe,
    AppComponent,
    PageNotFoundComponent,
    HomePageComponent,
    LoginPageComponent,
    SignupPageComponent,
    DashboardPageComponent,
    StartNewPageComponent,
    StartNowFormPageComponent,
    ApplicationsPageComponent,
    NgPdfPageSignComponent,
    UserPageComponent,
    UserComponent,
    NewUserComponent,
    UserViewComponent,
    DocCardComponent,
    LandingComponent,
    DocViewComponent,
    SettingComponents,
    UserEditComponent,
    FooterComponent,
    DragDropDirective,
    SafeUrlC
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialLibsModule,
    RouterModule.forRoot(appRoutes, {
      enableTracing: false // <-- debugging purposes only
      // onSameUrlNavigation: "reload"
    }),
    CommonUiModule
  ],
  providers: [
    AppContextService,
    AppCommonService,
    UserService,
    AuthService,
    UserPageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
