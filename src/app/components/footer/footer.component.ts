import { Component } from "@angular/core";
import { AppContextService } from "src/app/services/app-context.service";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"]
})
export class FooterComponent {
  year = new Date().getFullYear();

  // constructor(private appContext: AppContextService) {
  //   this.appContext.configLoader.subscribe(v => {
  //     if (v < 1) {
  //       return;
  //     }
  //     this.year = this.appContext.getCopyrightYear;
  //   });
  // }
}
