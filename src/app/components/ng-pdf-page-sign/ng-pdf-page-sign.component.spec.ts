import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgPdfPageSignComponent } from './ng-pdf-page-sign.component';

describe('NgPdfPageSignComponent', () => {
  let component: NgPdfPageSignComponent;
  let fixture: ComponentFixture<NgPdfPageSignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgPdfPageSignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgPdfPageSignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
