import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { AppContextService } from "src/app/services/app-context.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/services/auth.service";
import { v4 as uuid } from "uuid";
import { DocumentService } from "src/app/services/documents.service";
import { Router } from "@angular/router";
import { debounceTime } from "rxjs/operators";
import { UserPageService } from "src/app/services/usersPage.service";

declare var pdfjsLib: any;
export interface SignaturePoint {
  accountId: string;
  pageNo: number;
  x: number;
  y: number;
  isSelected?: boolean;
}
@Component({
  selector: "app-ng-pdf-page-sign",
  templateUrl: "./ng-pdf-page-sign.component.html",
  styleUrls: ["./ng-pdf-page-sign.component.scss"]
})
export class NgPdfPageSignComponent implements OnInit {
  //@ViewChild("mycanvas", { static: true }) Canvas: ElementRef;
  @Input() PdfData: string = null;
  @Input() SignatureListFromApi: any[] = [];
  @Input() readOnly: any = null;

  @Input() pdfCreatedAt: string;

  departments: string[] = [];
  docTypes: string[] = [];
  docTags: string[] = [];

  pageNumber: number = 1;
  totalPages: number = 1;
  currentPdf: any = null;
  currentPage: any = null;

  pageLoaded = false;
  scale: number = 1;
  selectedUserAccountId: string = null;
  selectedUserAccountPage: number = null;
  MouseUpListener: any = null;

  fetchedUsers: any = [];
  users: string[] = [
    "dinukasaminda@gmail.com",
    "lekana@gmail.com",
    "abcd@gmail.com"
  ];
  sings: SignaturePoint[] = [];
  pdfSize: { width: number; height: number } = { width: 0, height: 0 };
  fcUserSelect: string = "";
  fcUserInput = new FormControl(null);
  fcUserTag = new FormControl(null);
  docDetails = new FormGroup({
    fcDocName: new FormControl(null, Validators.required),
    fcDept: new FormControl(null, Validators.required),
    fcTypes: new FormControl(null, Validators.required),
    fcComment: new FormControl(null, Validators.required),
    fcTags: new FormControl(null, Validators.required)
  });
  drawContext = null;
  drawCanvas = null;
  constructor(
    private appContext: AppContextService,
    private authService: AuthService,
    private documentService: DocumentService,
    private userPageService: UserPageService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.appContext.configLoader.subscribe(v => {
      if (v < 1) {
        return;
      }
      this.departments = this.appContext.getLekanaDepartments;
      this.docTypes = this.appContext.getLekanaDocTypes;
      this.docTags = this.appContext.getLekanaDocTags;
    });

    try {
      const res = await this.userPageService.fetchUsers({
        idTerm: `*`,
        limit: 5,
        offset: 0
      });
      this.fetchedUsers = [...res.accounts];
    } catch (err) {
      this.appContext.showSnackBarShow("Unable to fetch users");
    }

    this.fcUserInput.valueChanges
      .pipe(debounceTime(100))
      .subscribe(async value => {
        try {
          const res = await this.userPageService.fetchUsers({
            idTerm: `${value}*`,
            limit: 5,
            offset: 0
          });
          this.fetchedUsers = [...res.accounts];
        } catch (err) {
          this.appContext.showSnackBarShow("Unable to fetch users");
        }
      });
  }
  ngAfterViewInit() {
    setTimeout(async () => {
      this.addListners();
      this.reloadPdf();
    }, 100);
  }

  /**
   * Execute when page change.
   */
  nextPageOfPdf() {
    this.loadPage(this.pageNumber + 1, this.pageNumber);
    this.pageNumber = this.pageNumber + 1;
  }

  /**
   * Execute when page change
   */
  previousPageOfPdf() {
    this.loadPage(this.pageNumber - 1, this.pageNumber);
    this.pageNumber = this.pageNumber - 1;
  }

  /**
   * Get the data url and render the pdf in canvas
   */
  async reloadPdf() {
    this.appContext.setProgressbarValue(true);
    try {
      if (this.PdfData == null) {
        throw "PDF file not valid";
      }
      this.PdfData = this.PdfData.split("↵").join("");
      if (!this.PdfData.startsWith("data:application/pdf;base64,")) {
        this.PdfData = "data:application/pdf;base64," + btoa(this.PdfData);
      }
      // localStorage.setItem("pdf-data", this.PdfData);
      const loadingTask = pdfjsLib.getDocument(this.PdfData);
      this.currentPdf = await loadingTask.promise;
      this.totalPages = this.currentPdf.numPages;
      this.loadPage(1, 1);
      // pdfjsLib
    } catch (err) {
      this.appContext.handleHTTPErrors(err);
    }
    this.appContext.setProgressbarValue(false);
  }

  /**
   * This function is responsible to handle pageChanges
   * @param pageNo - new page number
   * @param oldPage - old page number
   */
  async loadPage(pageNo: number, oldPage: number) {
    this.appContext.setProgressbarValue(true);

    try {
      this.currentPage = await this.currentPdf.getPage(pageNo);
      await this.pageChangeAndRedraw();
    } catch (err) {
      this.pageNumber = oldPage;
      this.appContext.handleHTTPErrors(err);
    }
    this.appContext.setProgressbarValue(false);
  }

  /**
   * Responsible for get the canvas context and store it in a local variable to use later
   * Add event listener to the canvas in edit mode
   */
  addListners() {
    const canvas: any = document.getElementById("my-pdf-canvas-221");
    this.drawCanvas = canvas;
    //const canvas: any = this.Canvas.nativeElement;
    this.drawContext = canvas.getContext("2d");
    this.drawContext.font = "20px Arial";

    if (!this.readOnly) {
      this.MouseUpListener = canvas.addEventListener("mouseup", evt => {
        const point = this.getMousePos(canvas, evt);
        this.reDrawSigns(true, point.x, this.pdfSize.height - point.y);
      });
    }
  }

  /**
   * Use this function to re draw the signatures after page change. call-site is loadPage()
   * @param scale - scale is a number
   */
  async pageChangeAndRedraw(scale: number = -1) {
    if (scale > 0) {
      this.scale = scale;
    }
    this.reDrawSigns(false);
  }

  /**
   * Responsible for draw signatures on canvas
   * @param withSign - false when change pages of the pdf or remove a sign from the list, otherwise true
   * @param x - signature position x axis
   * @param y - signature position y axis
   */
  async reDrawSigns(withSign: boolean, x: number = 0, y: number = 0) {
    try {
      if (withSign && this.selectedUserAccountId == null) {
        throw "Please select User";
      }
      this.drawContext.clearRect(0, 0, this.pdfSize.width, this.pdfSize.height);
      this.drawContext.beginPath();

      await this.DrawPage();

      let selectedFound = false;

      // in read only mode, loop through the signaturListFromApi array and draw sign for each item
      if (this.readOnly) {
        for (let i = 0; i < this.SignatureListFromApi.length; i++) {
          let sign = this.SignatureListFromApi[i];
          let x = (this.pdfSize.width * Number(sign.x)) / 100;
          let y = (this.pdfSize.height * Number(sign.y)) / 100;
          //console.log("YYYYY", y);
          const newElement: SignaturePoint = {
            accountId: sign.name,
            pageNo: sign.page,
            x: this.scaleNumber(x, this.pdfSize.width),
            y: this.scaleNumber(y, this.pdfSize.height),
            isSelected: true
          };
          this.sings.push(newElement);
          if (newElement.pageNo == this.pageNumber) {
            this.drawSign(newElement);
          }
        }
        return;
      }

      // loop through items in signs array to draw signatures in not readonly mode
      for (let i = 0; i < this.sings.length; i++) {
        const elmnt = this.sings[i];
        // if user already add a particular sign and then he change the position by click again on onother location update the item in the sign array
        if (
          withSign &&
          this.selectedUserAccountId != null &&
          elmnt.accountId == this.selectedUserAccountId &&
          elmnt.pageNo == this.pageNumber
        ) {
          // let x = (this.pdfSize.width * Number(elmnt.x)) / 100;
          // let y = (this.pdfSize.height * Number(elmnt.y)) / 100;
          //console.log("X IS", x, "Y IS", y);
          elmnt.x = this.scaleNumber(x, this.pdfSize.width);
          elmnt.y = this.scaleNumber(y, this.pdfSize.height);
          this.sings[i] = elmnt;
          selectedFound = true;
        }

        // check if the iteam in the array has the same page as current page and draw the signature
        if (elmnt.pageNo == this.pageNumber) {
          this.drawSign(elmnt);
        }
      }

      // if the user select a user from form for the first time and click on the pdf use this control structure
      if (
        withSign &&
        selectedFound == false &&
        this.selectedUserAccountId != null &&
        !this.readOnly
      ) {
        const newElement: SignaturePoint = {
          accountId: this.selectedUserAccountId,
          pageNo: this.pageNumber,
          x: this.scaleNumber(x, this.pdfSize.width),
          y: this.scaleNumber(y, this.pdfSize.height), // y from the bottom
          isSelected: true
        };
        this.sings.push(newElement);
        this.drawSign(newElement);
      }
    } catch (err) {
      this.appContext.handleHTTPErrors(err);
    }
  }

  /**
   * This function is responsible for draw a sign in canvas
   * @param sign - object of the signature type SignaturePoint
   */
  drawSign(sign: SignaturePoint) {
    const text = sign.accountId;
    const textWidth = this.drawContext.measureText(text).width;
    this.drawContext.fillStyle = "#1cd390";
    const vvx = this.reScaleNumber(sign.x, this.pdfSize.width);

    let yFromTop = 100 - sign.y;
    //console.log("YY ISS", yFromTop, sign.y);
    const vvy = this.reScaleNumber(yFromTop, this.pdfSize.height);
    this.drawContext.fillRect(vvx, vvy, textWidth + 25, 25);
    this.drawContext.fillStyle = "#fefefe";
    this.drawContext.fillText(text, vvx + 6, vvy + 12);
    this.drawContext.stroke();
  }

  /**
   * Render the pdf page inside the canvas context. This functions call in reDrawSignature() method
   */
  async DrawPage() {
    this.appContext.setProgressbarValue(true);

    try {
      var viewport = this.currentPage.getViewport({ scale: this.scale });

      // Prepare canvas using PDF page dimensions
      // const canvas: any = document.getElementById("my-pdf-canvas-221");
      // var context = canvas.getContext("2d");

      this.drawCanvas.height = viewport.height;
      this.drawCanvas.width = viewport.width;
      this.pdfSize = {
        width: viewport.width,
        height: viewport.height
      };
      // Render PDF page into canvas context
      var renderContext = {
        canvasContext: this.drawContext,
        viewport: viewport
      };
      await this.currentPage.render(renderContext).promise;
      this.pageLoaded = true;
    } catch (err) {
      this.appContext.handleHTTPErrors(err);
    }
    this.appContext.setProgressbarValue(false);
  }
  scaleNumber(n: number, max: number): number {
    return +((n * 100) / max).toFixed(4);
  }
  reScaleNumber(v: number, max: number): number {
    return (v * max) / 100;
  }

  setSelected(id) {
    this.fcUserSelect = id;
    this.selectedUserAccountId = id;
  }

  signatureList: any = null;
  saveSignatures() {
    let signsListStr: string = this.sings
      .map(item => {
        return `${item.accountId};${item.x};${item.y};${item.pageNo}`;
      })
      .join(",");
    this.signatureList = signsListStr;
  }
  getMousePos(canvas, evt): { x: number; y: number } {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
  }
  removeSign(id: string, pno: number) {
    let currentArray = [...this.sings];
    let newArray = currentArray.filter(sign => {
      return !(sign.accountId == id && sign.pageNo == pno);
    });
    this.sings = newArray;
    this.reDrawSigns(false);
  }

  onCustomTag() {
    let custom_tag: any = this.fcUserTag.value;
    let selectedTags: any[] = this.docDetails.value.fcTags;
    console.log("selected tags", selectedTags);
    if (selectedTags) {
      this.docTags.push(custom_tag);
      this.docDetails.patchValue({
        fcTags: [...selectedTags, custom_tag]
      });
    } else {
      this.docTags.push(custom_tag);
      this.docDetails.patchValue({
        fcTags: [custom_tag]
      });
    }

    this.fcUserTag.setValue(null);
  }

  async onSubmit() {
    this.appContext.setProgressbarValue(true);
    this.saveSignatures();
    try {
      if (!this.docDetails.valid) {
        throw "Please fill all fields";
      }
      if (!this.signatureList) {
        throw "Please mark all user's signatures";
      }

      const dataBlob = this.PdfData.replace("data:application/pdf;base64,", "");
      console.log(dataBlob);
      //console.log(btoa(this.PdfData));
      let ob = {
        id: "" + Date.now(),
        execer: this.authService.userAccount.value.id,
        messageType: "create",
        documentId: "" + uuid(),
        documentCreator: this.authService.userAccount.value.id,
        documentName: this.docDetails.value.fcDocName,
        documentDescription: this.docDetails.value.fcComment,
        documentCompany: "rahasak",
        documentDept: this.docDetails.value.fcDept,
        documentTyp: this.docDetails.value.fcTypes,
        documentBlob: dataBlob,
        documentTags: this.docDetails.value.fcTags.toString(),
        documentVerifier: "",
        documentSigners: this.signatureList,
        documentMasks: "",
        documentParent: ""
      };
      await this.documentService.create(ob);
      this.appContext.showSnackBarShow("Document Uploaded", "SUCCESS");
      this.router.navigate(["app/applications"]);
    } catch (err) {
      this.appContext.showSnackBarShow(err);
    }
    this.appContext.setProgressbarValue(false);
  }
  openPdf() {
    console.log(this.PdfData);
    let dataUrl = "" + this.PdfData;
    dataUrl = dataUrl.replace("data:application/pdf;base64,", "");
    let pdfWindow = window.open();

    pdfWindow.document.write(
      "<style>body{margin:0px;}</style> <object  width='100%' height='100%' data='data:application/pdf;base64, " +
        encodeURI(dataUrl) +
        "'></object>"
    );
    pdfWindow.document.close();
  }

  getDate(utc: string) {
    return new Date(utc).toDateString();
  }

  ngOnDestroy() {
    //remove listenrs mouseup
    try {
      const canvas: any = document.getElementById("my-pdf-canvas-221");
      canvas.removeEventListener("mouseup", this.MouseUpListener);
    } catch (err) {
      console.error(err);
    }
  }
}
//--------------------------------------------------
// <div class="main-cont">
//   <div class="search-view-adsc1" id="ad-cont-left">

// ngAfterViewInit(): void {
//     setTimeout(() => {
//       if (window.innerWidth > 990) {
//         window.addEventListener('scroll', this.myScrollEvent, true);
//       }
//     }, 500);
// }

// myScrollEvent(evt: any) {
//   try {
//     let container_element = document.getElementById('ad-cont-left');
//     let element = document.getElementById('search-left-verticle');
//     let footer = document.getElementById('result-go-to-top');
//     let footer_rect = footer.getBoundingClientRect();
//     // console.log('footer_rect', footer_rect);
//     // console.log('ss height', window.innerHeight);
//     if (window.innerHeight > footer_rect.top) {
//       element.style.position = 'fixed';
//       element.style.top = 70 + footer_rect.top - window.innerHeight + 'px';
//       // console.log(window.innerHeight - footer_rect.top + 'px');
//     } else {
//       let rect: any = container_element.getBoundingClientRect();
//       if (rect.y < 0) {
//         element.style.position = 'fixed';
//         element.style.top = '70px';
//       } else {
//         element.style.position = 'unset';
//       }
//       // console.log('ad', rect);
//     }
//   } catch (err) {
//     console.log(err);
//   }
// }

// ngOnDestroy(): void {
//   //Called once, before the instance is destroyed.
//   //Add 'implements OnDestroy' to the class.
//   if (isPlatformBrowser(this.platformId)) {
//     window.removeEventListener('scroll', this.myScrollEvent, true);
//   }
// }
