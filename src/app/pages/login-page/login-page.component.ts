import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { AppContextService } from 'src/app/services/app-context.service';
import { EMAIL_REGEX } from 'src/app/utils/Utils';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginFormGroup = new FormGroup({
    email: new FormControl(null, [
      Validators.required,
      Validators.pattern(EMAIL_REGEX)
    ]),
    password: new FormControl(null, [Validators.required]),
    remember: new FormControl(true, [Validators.required])
  });
  remember_me = true;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private appContextService: AppContextService,
    private router: Router
  ) {}

  async ngOnInit() {
    const isLoggedIn = await this.authService.authCheck();
    if(isLoggedIn) {
      this.router.navigate(['app'])
    }
  }
  async login() {
    //this.router.navigate(['account']);
    try {
      if (this.loginFormGroup.valid) {
        const value = this.loginFormGroup.value;
        const response = await this.userService.loginUser(value.email, value.password);
        //const user = await this.userService.accountFindByEmail(value.email);
        const resObject = JSON.parse(atob(response["token"]));
        const userRole = resObject.roles.toLowerCase();

        if(typeof userRole !== "undefined" && resObject.roles === "admin" ) {
          const token = resObject.digsig
          const email = resObject.id
          const user : {id:string,role:string} = {
            id : email.toString(),
            role : userRole.toString()
          }
          this.authService.authToken.next(token);
          this.authService.userAccount.next(user);
          this.authService.authState.next(1);
          //console.log([user, token]);
          await this.authService.saveUsersLocalStorage(user, token);
          this.router.navigate(['app']);
        } else {
          this.appContextService.showSnackBarShow('Access Denied!', null, 3000, 'warn-panel');
        }
      } else {
        this.appContextService.showSnackBarShow('Email/Password required', null, 3000, 'warn-panel');
      }
    } catch (err) {
      if (typeof err == 'string') {
        this.appContextService.showSnackBarShow(err, null, 3000, 'warn-panel');
      } else {
        this.appContextService.showSnackBarShow(
          'Email or Password not valid',
          null,
          3000,
          'warn-panel'
        );
      }
    }
  }
}
