import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MustMatch } from 'src/app/utils/ControlValidator';
import { UserService } from 'src/app/services/user.service';
import { AppContextService } from 'src/app/services/app-context.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector:"app-setting-select",
    templateUrl:"./setting.component.html",
    styleUrls:["./setting.component.scss"]
})
export class SettingComponents implements OnInit {
    passwordChange : FormGroup;
    submitted = false;
    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private authService: AuthService,
        private appContextService: AppContextService) {}

    ngOnInit() {
        this.passwordChange = this.formBuilder.group({
            current : new FormControl(null, Validators.required),
            new : new FormControl(null, Validators.required),
            confirm : new FormControl(null, Validators.required)
        }, {
            validator: MustMatch('new', 'confirm')
        })
    }

    get f() { return this.passwordChange.controls; }

    async onSubmit() {
        this.submitted = true;

        if (this.passwordChange.invalid) {
            return;
        }
        try {
            await this.userService.changePassword(this.passwordChange.value.current, this.passwordChange.value.new)
            await this.authService.signout();
            this.appContextService.showSnackBarShow('Password Changed', 'SUCCESS')
            //this.router.navigate(['/login']);
            window.location.href = '/login'
        }catch(err) {
            this.appContextService.showSnackBarShow('Password mismatch');
        }
    }
}