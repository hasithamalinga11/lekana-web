import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartNowFormPageComponent } from './start-now-form-page.component';

describe('StartNowFormPageComponent', () => {
  let component: StartNowFormPageComponent;
  let fixture: ComponentFixture<StartNowFormPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartNowFormPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartNowFormPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
