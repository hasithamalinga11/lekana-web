import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef
} from "@angular/core";
import { convertToUFirstWordsStyle } from "src/app/utils/Utils";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { AppContextService } from "src/app/services/app-context.service";
import { FormGroup, FormControl, NgForm, Validators } from "@angular/forms";
import { getUUID } from "src/app/utils/DateTimeUtils";
import { UserPageService } from "src/app/services/usersPage.service";
import { debounceTime } from "rxjs/operators";
import { DocumentService } from "src/app/services/documents.service";
import { AuthService } from "src/app/services/auth.service";
import { v4 as uuid } from "uuid";

@Component({
  selector: "app-start-now-form-page",
  templateUrl: "./start-now-form-page.component.html",
  styleUrls: ["./start-now-form-page.component.scss"]
})
export class StartNowFormPageComponent implements OnInit {
  docTags: string[] = null;
  formState = 1;
  pageNumber = 1;
  departments: any[] = [];
  fileElement: any = null;
  file: any = {
    fileData: null,
    dataurl: null
  };
  current_doc_tag: any = null;

  //data from doc view page are assign to here via history object. see doc-view component
  // docDataForUpdate : any = null;

  @ViewChild("reader", { static: false }) pdfReader;
  @ViewChild("pdfContainer", { static: false }) pdfContainer;
  @ViewChild("signPos", { static: false }) signPos;

  users: FormControl;
  fetchedUsers: any = [];
  selectedUser: any = null;

  signatures: {
    uid?: string;
    X?: number;
    Y?: number;
    page?: number;
    hasSigned?: boolean;
    parentWidth?: number;
    parentHeight?: number;
  }[] = [];

  progressVisible = false;
  imageLoading = false;
  uploadPresntage = 0;

  modelv = false;
  modal_style = {
    visibility: "hidden",
    opacity: 0
  };
  model_title = "";
  model_data = "";
  physician_id: string = null;
  current_application: any = {};
  processing = false;

  docForm = new FormGroup({
    docName: new FormControl(null, Validators.required),
    dept: new FormControl("selected", Validators.required),
    typ: new FormControl(null, Validators.required),
    docDesc: new FormControl(null),
    tags: new FormControl(null, Validators.required)
  });

  constructor(
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private userPageService: UserPageService,
    private appContextService: AppContextService,
    private documentService: DocumentService,
    private authService: AuthService,
    private router: Router,
    private changeDetector: ChangeDetectorRef
  ) {}

  refreshId: string = null;

  cordX: string = null;
  cordY: string = null;
  isMarkXY = false;
  showCircle: any = {};
  ngOnInit() {
    this.appContextService.configLoader.subscribe(v => {
      if (v < 1) {
        return;
      }
      this.departments = this.appContextService.getLekanaDepartments;
      this.docTags = this.appContextService.getLekanaDocTags;
    });

    this.file.dataurl = localStorage.getItem("pdf-data");
    this.formState = 1;
    this.users = new FormControl(null);
    this.users.valueChanges.pipe(debounceTime(400)).subscribe(async value => {
      // console.log(value);
      try {
        const res = await this.userPageService.fetchUsers({
          idTerm: `${value}*`,
          limit: 5,
          offset: 0
        });
        this.fetchedUsers = [...res.accounts];
      } catch (err) {
        this.appContextService.showSnackBarShow("Unable to fetch users");
      }
    });
    this.activatedRoute.queryParams.subscribe(map => {
      if (map.refreshId) {
        if (this.refreshId != map.refreshId) {
          // this.refreshPage();
        }
        this.refreshId = map.refreshId;
      }
    });
  }
  refreshPage() {
    this.docForm.reset();
    this.formState = 1;
    this.fetchedUsers = [];
    this.signatures = [];
  }

  next() {
    this.formState += 1;
  }
  back() {
    this.formState -= 1;
  }

  nextPageOfPdf() {
    this.pageNumber = this.pageNumber + 1;
  }
  previousPageOfPdf() {
    this.pageNumber = this.pageNumber - 1;
  }

  getMouseLocation(e: MouseEvent) {
    if (this.selectedUser) {
      //console.log(this.pdfContainer);
      //console.log("scrollx", e.pageX);
      //console.log("scrolly", e.pageY);
      this.cordY = `${e.pageY}px`;
      this.cordX = `${e.pageX}px`;
      this.isMarkXY = true;
      // this.showCircle= {
      //   'position' : 'absolute',
      //   'width' : '200px',
      //   'height' : '50px',
      //   'border-radius' : '50%',
      //   'border-top-left-radius' : '0',
      //   'background-color' : '#1cd390',
      //   'opacity' : '1',
      //   'text-align': 'center',
      //   'vertical-align': 'middle',
      //   'line-height': '50px',
      //   'font-weight' : '700',
      //   'font-family' : "'Lato', sans-serif",
      //   'font-size' : '1rem',
      //   'color' : 'white'
      // }
      let pdfContainerbox = this.pdfReader.element.nativeElement.childNodes[0];
      let pdfElement = this.pdfReader.element.nativeElement.childNodes[0]
        .childNodes[0].childNodes[0];
      // console.log('---------------------')
      // console.log("container", pdfContainerbox);
      // console.log("height", pdfElement.clientHeight);
      // console.log("width", pdfElement.clientWidth);
      // console.log("offsetLeft", pdfElement.offsetLeft);
      // console.log("offsetTop", pdfElement.offsetTop);

      let xPos =
        ((e.pageX - this.pdfContainer.nativeElement.offsetLeft) /
          pdfElement.clientWidth) *
        100;
      let yPos =
        100 -
        ((e.pageY - this.pdfContainer.nativeElement.offsetTop) /
          pdfElement.clientHeight) *
          100;

      //console.log("XX", xPos);
      //console.log("YY", yPos);
      let user = this.signatures.find(sing => sing.uid === this.selectedUser);
      let index = this.signatures.indexOf(user);
      user.X = xPos;
      user.Y = yPos;
      user.page = this.pageNumber;
      user.hasSigned = true;
      user.parentHeight = pdfElement.clientHeight;
      user.parentWidth = pdfElement.clientWidth;
      this.signatures[index] = { ...user };
      // console.log("this signatures", this.signatures);
    } else {
      this.appContextService.showSnackBarShow(
        "please select a user before add signatures"
      );
    }
  }

  browseFile(fileInput: any, tag: string) {
    //console.log("browseFile");
    if (this.fileElement == null) {
      this.fileElement = fileInput;
    }
    this.current_doc_tag = tag;
    fileInput.click();
  }

  uploadFile(event) {
    const element = event[0];
    if (element.type === "application/pdf") {
      this.file.fileData = element;
      this.readURL();
    } else if (element.size > 1024 * 1024 * 10) {
      this.appContextService.showSnackBarShow("Max size is 10mb");
    } else {
      this.appContextService.showSnackBarShow("Only pdf are valid to upload");
    }
  }

  readURL(): void {
    if (this.file.fileData != null) {
      const reader = new FileReader();
      reader.onload = e => {
        this.file.dataurl = reader.result;
        console.log(this.file.dataurl);
      };
      reader.onloadend = e => {
        this.next();
      };
      reader.readAsDataURL(this.file.fileData);
    }
  }

  setSelected(id) {
    this.selectedUser = id;
    this.signatures.push({
      uid: id,
      X: null,
      Y: null,
      page: this.pageNumber,
      hasSigned: false
    });
    this.fetchedUsers = [];
  }

  setSelectedFromList(id) {
    this.selectedUser = id;
  }

  isSubmiting = false;
  async submit() {
    this.isSubmiting = true;
    //check if signature list has not signed item
    let hasNotSigned = this.signatures
      .map(sign => sign.hasSigned)
      .indexOf(false);
    try {
      if (!this.signatures && hasNotSigned > 0) {
        this.appContextService.showSnackBarShow(
          "Please mark all user's signatures"
        );
        this.isSubmiting = false;
      }
      if (!this.docForm.valid) {
        this.appContextService.showSnackBarShow(
          "Please fill the form correctly"
        );
        this.isSubmiting = false;
      }
      const docFormValue = this.docForm.value;
      if (docFormValue.dept == "selected") {
        throw "Department not valid";
      }
      let signProperty = [];
      this.signatures.map(sign => {
        signProperty.push(`${sign.uid};${sign.X};${sign.Y};${sign.page}`);
      });

      let ob = {
        id: "" + Date.now(),
        execer: this.authService.userAccount.value.id,
        messageType: "create",
        documentId: "" + uuid(),
        documentCreator: this.authService.userAccount.value.id,
        documentName: this.docForm.value.docName,
        documentDescription: this.docForm.value.docDesc,
        documentCompany: "",
        documentDept: this.docForm.value.dept,
        documentTyp: this.docForm.value.typ,
        documentBlob: btoa(this.file.dataurl),
        documentTags: this.docForm.value.tags.toString(),
        documentVerifier: "",
        documentSigners: signProperty.toString(),
        documentMasks: "",
        documentParent: ""
      };
      await this.documentService.create(ob);
      this.isSubmiting = false;
      this.appContextService.showSnackBarShow("Document Uploaded", "SUCCESS");
      this.router.navigate(["app/applications"]);
    } catch (err) {
      // "Failed to submit. Please try again"
      this.appContextService.handleHTTPErrors(err);
      this.isSubmiting = false;
    }
  }
}
