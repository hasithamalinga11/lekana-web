import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartNewPageComponent } from './start-new-page.component';

describe('StartNewPageComponent', () => {
  let component: StartNewPageComponent;
  let fixture: ComponentFixture<StartNewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartNewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartNewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
