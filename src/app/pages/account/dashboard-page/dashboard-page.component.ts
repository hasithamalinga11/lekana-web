import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import { AppContextService } from "src/app/services/app-context.service";
import { IUser } from "src/app/models/IUser";

@Component({
  selector: "app-dashboard-page",
  templateUrl: "./dashboard-page.component.html",
  styleUrls: ["./dashboard-page.component.scss"]
})
export class DashboardPageComponent implements OnInit {
  user: IUser = null;
  year: any = new Date().getFullYear();
  constructor(
    private router: Router,
    private authService: AuthService,
    private appContextService: AppContextService
  ) {
    authService.userAccount.subscribe(user => (this.user = user));
  }

  ngOnInit() {}
  logout() {
    this.authService.signout();
    window.location.href = '/login'
    //this.router.navigate(["login"]);
  }

  goUplaodPage() {
    this.router.navigate(["/app/application"], {
      queryParams: { refreshId: "" + Date.now() }
    });
  }
}
