import { Component, OnInit } from "@angular/core";
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

 @Component({
     selector: 'app-landing',
     templateUrl: './landing.component.html',
     styleUrls: ['./landing.component.scss']
 })
 export class LandingComponent implements OnInit {
     constructor(
         private authService: AuthService,
         private userSerivce: UserService,
         private router: Router
     ){}
     async ngOnInit(){
        const isLoggedIn = await this.authService.authCheck();
        if(isLoggedIn) {
          this.router.navigate(['app'])
        }
     }
 }
