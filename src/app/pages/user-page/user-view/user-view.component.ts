import { Component, OnInit, Input } from "@angular/core";
import { AppContextService } from "src/app/services/app-context.service";
import { Router, ActivatedRoute } from "@angular/router";
import { UserPageService } from "src/app/services/usersPage.service";
import { Subscription } from "rxjs/internal/Subscription";

@Component({
  selector: "app-user-view",
  templateUrl: "./user-view.component.html",
  styleUrls: ["./user-view.component.scss"]
})
export class UserViewComponent implements OnInit {
  data: any = {};
  user: any = {};
  newRole: string = null;
  isFetching = false;

  selectedAccontId: string = null;
  querySub: Subscription = null;
  constructor(
    private router: Router,
    private userService: UserPageService,
    private appContextService: AppContextService,
    private activatedRoute: ActivatedRoute
  ) {}
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(map => {
      if (map.accountId) {
        this.selectedAccontId = map.accountId;
          this.fetchUsers(1, 0, this.selectedAccontId);
      } else {
        this.appContextService.handleHTTPErrors("User id invalid!", true);
      }
    });
  }
  async fetchUsers(l: number, o: number, i: string = "*") {
    this.appContextService.setProgressbarValue(true);
    try{
      const res = await this.userService.fetchUsers({
        limit: l,
        offset: o,
        idTerm: i
      });
      this.user = res.accounts[0];
    }catch(err){
      this.appContextService.handleHTTPErrors(
        { error: err },
        true,
        null,
        "warn-panel"
      );
    }
    this.appContextService.setProgressbarValue(false);
  }
  getDate(utc) {
    return new Date(utc).toDateString();
  }
  ngOndDestry() {
    if (this.querySub) {
      this.querySub.unsubscribe();
    }
    //console.log("destroy");
  }
}
