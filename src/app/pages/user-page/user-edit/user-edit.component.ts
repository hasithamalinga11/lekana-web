import { Component, OnInit, Input } from "@angular/core";
import { AppContextService } from "src/app/services/app-context.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { UserPageService } from "src/app/services/usersPage.service";
import { Subscription } from "rxjs/internal/Subscription";

@Component({
  selector: "app-user-edit",
  templateUrl: "./user-edit.component.html",
  styleUrls: ["./user-edit.component.scss"]
})
export class UserEditComponent implements OnInit {
  data: any = {};
  user: any = {};
  newRole: string = null;
  isFetching = false;

  departments: string[] = [];
  roles: string[] = [];
  companies: string[] = [];

  selectedAccontId: string = null;
  querySub: Subscription = null;

  userUpdateForm = new FormGroup({
    username: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.email]),
    phone: new FormControl(null, [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10)
    ]),
    role: new FormControl(null, Validators.required),
    dept: new FormControl(null),
    company: new FormControl(null, Validators.required)
  });

  constructor(
    private router: Router,
    private userService: UserPageService,
    private appContextService: AppContextService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.appContextService.configLoader.subscribe(v => {
      if (v < 1) {
        return;
      }
      this.departments = this.appContextService.getLekanaDepartments;
      this.companies = this.appContextService.getCompanies;
      this.roles = this.appContextService.getRoles;
    });

    this.activatedRoute.queryParams.subscribe(async map => {
      if (map.accountId) {
        this.selectedAccontId = map.accountId;
        try {
          await this.fetchUsersAndUpdateForm(1, 0, this.selectedAccontId);
        } catch (err) {
          this.appContextService.handleHTTPErrors(err, true);
        }
        this.appContextService.setProgressbarValue(false);
      } else {
        this.appContextService.handleHTTPErrors("User id invalid!", true);
      }
    });
  }

  async fetchUsersAndUpdateForm(l: number, o: number, i: string = "*") {
    this.appContextService.setProgressbarValue(true);
    const res = await this.userService.fetchUsers({
      limit: l,
      offset: o,
      idTerm: i
    });
    let user = res.accounts[0];

    this.userUpdateForm.patchValue({
      username: user.name,
      email: user.email,
      phone: user.phone,
      role: user.roles || null,
      dept: null, //department is not in response
      company: user.company || null
    });
    this.appContextService.setProgressbarValue(false);
  }

  async onSubmit() {
    console.log(this.userUpdateForm.value);
    this.appContextService.setProgressbarValue(true);
    try {
      if (!this.userUpdateForm.valid) {
        throw "Form is not valid";
      }
      await this.userService.updateUser(this.userUpdateForm.value);
      await this.userService.addRole(this.userUpdateForm.value);
      this.router.navigate(["app/users"]);
      this.appContextService.showSnackBarShow("User Updated", "SUCCESS");
    } catch (err) {
      this.appContextService.showSnackBarShow(err);
      //handle error
    }
    this.appContextService.setProgressbarValue(false);
  }

  // async addRole() {
  //   try {
  //     this.isFetching = true;
  //     await this.userService.addRole(this.userUpdateForm.value);
  //     this.data.roles = this.newRole;
  //     this.isFetching = false;
  //   } catch (err) {
  //     this.appContextService.showSnackBarShow("Unable to add role. Try again");
  //     this.isFetching = false;
  //   }
  // }

  ngOndDestry() {
    if (this.querySub) {
      this.querySub.unsubscribe();
    }
    //console.log("destroy");
  }
}
