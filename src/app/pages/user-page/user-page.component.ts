import { Component, OnInit } from "@angular/core";
import { UserPageService } from "../../services/usersPage.service";
import { NewUser } from "../../dto/newUser.dto";
import { AppContextService } from "src/app/services/app-context.service";

@Component({
  selector: "app-user-page",
  templateUrl: "./user-page.component.html",
  styleUrls: ["./user-page.component.scss"]
})
export class UserPageComponent implements OnInit {
  users: NewUser[] = [];
  meta: { count?: number; limit?: number; offset?: number; total?: number } = {
    count: null,
    limit: null,
    offset: null,
    total: null
  };
  searchTerm = "";
  pageSize = 15;
  currentPage = 0;

  constructor(
    private usersService: UserPageService,
    private appContextService: AppContextService
  ) {

  }

  ngOnInit() {
    this.fetchUsers(this.pageSize, 0, "*");
  }

  async handlePage(e: any) {
    this.appContextService.setProgressbarValue(true);
    let limit = e.pageSize;
    let offset = limit * e.pageIndex;
    try {
      await this.fetchUsers(limit, offset, this.searchTerm.toLowerCase());
    } catch (err) {
      // this.appContextService.handleHTTPErrors(err, true, 'ERROR',)
    }
    this.appContextService.setProgressbarValue(false);
  }

  async searchUsers(e: any) {
    this.appContextService.setProgressbarValue(true);
    let term = `${this.searchTerm}*`;
    await this.fetchUsers(this.pageSize, 0, term.toLowerCase());
    this.appContextService.setProgressbarValue(false);
  }

  async fetchUsers(l: number, o: number, i: string = "*") {
    this.appContextService.setProgressbarValue(true);
    const res = await this.usersService.fetchUsers({
      limit: l,
      offset: o,
      idTerm: i
    });

    this.users = [...res.accounts];
    this.meta = res.meta;
    this.appContextService.setProgressbarValue(false);
  }
}
