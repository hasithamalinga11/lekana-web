import { Component, OnInit } from "@angular/core";
import { NgForm, FormGroup, FormControl, Validators } from "@angular/forms";
import { UserPageService } from "src/app/services/usersPage.service";
import { Router } from "@angular/router";
import { AppContextService } from "src/app/services/app-context.service";

@Component({
  selector: "app-edit-user",
  templateUrl: "./new-user.component.html",
  styleUrls: ["./new-user.component.scss"]
})
export class NewUserComponent implements OnInit {
  isSubmiting = false;

  departments: string[] = [];
  roles: string[] = [];
  companies: string[] = [];

  userForm = new FormGroup({
    username: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.email]),
    phone: new FormControl(null, [
      Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10)
    ]),
    role: new FormControl(null, Validators.required),
    dept: new FormControl(null, Validators.required),
    company: new FormControl(null, Validators.required)
  });

  constructor(
    private userPageService: UserPageService,
    private router: Router,
    private appContextService: AppContextService
  ) {}

  ngOnInit() {
    this.appContextService.configLoader.subscribe(v => {
      if (v < 1) {
        return;
      }
      this.departments = this.appContextService.getLekanaDepartments;
      this.companies = this.appContextService.getCompanies;
      this.roles = this.appContextService.getRoles;
    });
  }

  async onSubmit() {
    console.log(this.userForm.value);
    this.appContextService.setProgressbarValue(true);
    try {
      if (!this.userForm.valid) {
        throw "Form is not valid";
      }
      await this.userPageService.createUser(this.userForm.value);
      this.router.navigate(["app/users"]);
      this.appContextService.showSnackBarShow("User Created", "SUCCESS");
    } catch (err) {
      this.appContextService.showSnackBarShow(err);
      //handle error
    }
    this.appContextService.setProgressbarValue(false);
  }
}
