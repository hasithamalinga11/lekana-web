import { Component, OnInit, Input } from "@angular/core";
import { NewUser } from "../../../dto/newUser.dto";
import { UserPageService } from "src/app/services/usersPage.service";
import { Router, ActivatedRoute } from "@angular/router";
import { AppContextService } from "src/app/services/app-context.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {
  @Input() user: NewUser = {
    id: null,
    name: null,
    phone: null,
    roles: null
  };

  style = {};
  constructor(
    private userPageService: UserPageService,
    private router: Router,
    private appContextService: AppContextService
  ) {}
  ngOnInit() {
    //console.log('user is', this.user);
  }
  isDeleting = false;
  deleteUser() {
    this.appContextService.showSnackBarListern("Are you sure to delete user ?","DELETE",3000).subscribe(async v=>{
      if(v) {
        try {
          this.isDeleting = true;
          await this.userPageService.deleteUser(this.user.id);
          this.isDeleting = false;
          this.style = {
            display: "none"
          };
        } catch (err) {
          this.appContextService.showSnackBarShow(
            "Something went wrong. Please try again"
          );
          this.isDeleting = false;
        }
      }
    })
  }
}
