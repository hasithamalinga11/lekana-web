import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { AppContextService } from 'src/app/services/app-context.service';
import { Router } from '@angular/router';
import { MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {
  registrationFormGroup = new FormGroup({
    email: new FormControl('dinukasaminda@gmail.com', [Validators.required]),
    username: new FormControl('dinukasaminda', [Validators.required]),
    password: new FormControl('12345', [Validators.required]),
    password_confirm: new FormControl('12345', [Validators.required])
  });
  constructor(
    private userService: UserService,
    private appContextService: AppContextService,
    private router: Router
  ) {}

  ngOnInit() {}
  async registerUser() {
    // this.appContextService.setProgressbarValue(true);
    // try {
    //   if (this.registrationFormGroup.invalid) {
    //     throw 'All Fields required';
    //   }
    //   const value = this.registrationFormGroup.value;
    //   if (value.password.length < 4 || value.password != value.password_confirm) {
    //     throw 'Passwords not match';
    //   }
    //   const data = await this.userService.registeUser(
    //     value.email,
    //     value.username,
    //     value.password,
    //     'doctor'
    //   );
    //   this.appContextService.handleHTTPErrors('Account Created', true, null, 'success-panel');
    //   console.log(data);
    // } catch (err) {
    //   console.log(err);
    //   this.appContextService.handleHTTPErrors(err, true, null, 'warn-panel');
    // }
    // this.appContextService.setProgressbarValue(false);
  }
}
