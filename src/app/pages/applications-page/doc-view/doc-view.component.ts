import { Component, OnInit, Input } from "@angular/core";
import { AppContextService } from "src/app/services/app-context.service";
import { Router, ActivatedRoute } from "@angular/router";
import { UserPageService } from "src/app/services/usersPage.service";
import { DocumentService } from "src/app/services/documents.service";
declare var pdfjsLib: any;
@Component({
  selector: "app-doc-view",
  templateUrl: "./doc-view.component.html",
  styleUrls: ["./doc-view.component.scss"]
})
export class DocViewComponent implements OnInit {
  docID: any = null;
  data: any = null;
  pdf: any = null;
  PdfData: any = null;
  isFetching = false;

  signUsers: string[] = null;
  comments: any[] = null;

  pageNumber: number = 1;
  totalPages: number = 1;
  currentPdf: any = null;
  currentPage: any = null;

  pageLoaded = false;
  scale: number = 1;

  signatureList: any[] = [];

  pdfSize: { width: number; height: number } = { width: 0, height: 0 };
  drawContext = null;
  constructor(
    private router: Router,
    private documentService: DocumentService,
    private appContext: AppContextService,
    private activatedRoute: ActivatedRoute
  ) {}

  async ngOnInit() {
    this.activatedRoute.queryParams.subscribe(map => {
      if (map.id) {
        this.docID = map.id;
        this.getDoc();

        window.scrollTo(0, 0);
      }
    });
  }

  async getDoc() {
    this.appContext.setProgressbarValue(true);
    try {
      this.isFetching = true;
      const res: any = await this.documentService.getPdf(this.docID);
      this.pdf = res;
      //console.log("RES IS", res);
      // let base64 = btoa(str.replace(/[\u00A0-\u2666]/g, function(c) {
      //     return '&#' + c.charCodeAt(0) + ';';
      // }));
      console.log("Recieved pdf", this.pdf.blob);
      this.PdfData = atob(this.pdf.blob);
      this.signUsers = this.pdf.signers.split(",");
      this.comments = this.pdf.signatures;
      this.signatureList = res.signatureAnnotations;
      console.log("SIGNATUR COPIED", this.signatureList);
      this.isFetching = false;
    } catch (err) {
      this.isFetching = false;
      this.appContext.showSnackBarShow(err);
    }
    this.appContext.setProgressbarValue(false);
  }

  getBackground() {
    if (this.pdf.status === "approved") {
      return "#1cd390";
    } else if (this.pdf.status === "verified") {
      return "#fe9800";
    } else {
      return "maroon";
    }
  }
  setStatus() {
    if (this.pdf.status === "verified") {
      return "pending";
    } else {
      return this.pdf.status;
    }
  }
  setSignColor(sign: string) {
    let found = this.comments.filter(item => item.signer === sign);
    if (found.length > 0) {
      return "#1cd390";
    } else {
      return "#b2b2b2";
    }
  }

  getDate(utc: string) {
    return new Date(utc).toDateString();
  }
}
