import { Component, OnInit, Input } from "@angular/core";
import { DocumentListItem } from '../../../interfaces/document.interface';
import { Router } from '@angular/router';
@Component({
    selector:"app-doc-card",
    templateUrl:"./doc-card.component.html",
    styleUrls:["./doc-card.component.scss"]
})
export class DocCardComponent implements OnInit {
    @Input() docDetail : DocumentListItem; 
    constructor(private router:Router){}
    ngOnInit(){
        //console.log(this.docDetail)
    }
    showPdf(){
        this.router.navigate([])
    }
}