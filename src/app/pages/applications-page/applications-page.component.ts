import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { AppContextService } from "src/app/services/app-context.service";
import { Router } from "@angular/router";
import { FormControl, FormGroup } from "@angular/forms";
import { debounceTime } from "rxjs/operators";
import { cloneObject } from "src/app/utils/Utils";
import { DocumentService } from "src/app/services/documents.service";
import { DocSearch } from "src/app/interfaces/docSearch.interface";

@Component({
  selector: "app-applications-page",
  templateUrl: "./applications-page.component.html",
  styleUrls: ["./applications-page.component.scss"]
})
export class ApplicationsPageComponent implements OnInit {
  // fc_keyword = new FormControl('');
  // fc_doc_status = new FormControl('approved');
  searchForm: FormGroup;
  documents: any[] = [];
  departments: any[] = [];
  companies: any[] = [];
  types: any[] = [];
  meta: any = {};
  pageSize = 15;
  currentPage = 0;
  //last_key: string = null;
  selected_application: any = {};
  total = 0;
  processing = false;
  constructor(
    private documentService: DocumentService,
    private appContextService: AppContextService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.appContextService.configLoader.subscribe(v => {
      if (v < 1) {
        return;
      }
      this.departments = this.appContextService.getLekanaDepartments;
      this.companies = this.appContextService.getCompanies;
      this.types = this.appContextService.getLekanaDocTypes;
    });

    this.searchForm = new FormGroup({
      keyword: new FormControl(null),
      dept: new FormControl(null),
      typ: new FormControl(null),
      status: new FormControl(null)
    });
    await this.fetchDocuments({});
    this.searchForm.valueChanges.pipe(debounceTime(400)).subscribe(val => {
      this.fetchDocuments({
        idTerm: `${val.keyword}*`,
        dept: val.dept,
        typ: val.typ,
        status: val.status
      });
    });
    // this.openModel(true) ;
    // this.fc_keyword.valueChanges.pipe(debounceTime(400)).subscribe(value => {
    //   console.log(value);
    //   this.fetchDocuments();
    // });
    // this.fc_doc_status.valueChanges.pipe(debounceTime(400)).subscribe(value => {
    //   console.log(value);
    //   this.fetchDocuments();
    // });
    //await this.fetchDocuments({});
    //console.log("documents", this.documents);
  }
  async fetchDocuments(opt: DocSearch) {
    this.appContextService.setProgressbarValue(true);
    try {
      const result = await this.documentService.fetchDocuments(opt);
      this.documents = [...result.documents];
      this.meta = { ...result.meta };
      //this.last_key = result.lastKey;
      // this.documents = result.credentials.map((item: any) => {
      //   item.idx = iv++;
      //   item.uplaoded_on = transformDateTime(
      //     item.approveTime.split(' ')[0],
      //     'YYYY-MM-DD',
      //     'MM.DD.YY'
      //   );
      //   return item;
      // });

      this.total = this.documents.length;
    } catch (err) {
      //console.log(err);
      this.appContextService.handleHTTPErrors(
        { error: err },
        true,
        null,
        "warn-panel"
      );
    }
    this.appContextService.setProgressbarValue(false);
  }
  modelv = false;
  modal_style = {
    visibility: "hidden",
    opacity: 0
  };

  async handlePage(e: any) {
    //console.log(e);
    //console.log(this.meta);
    let limit = e.pageSize;
    let offset = limit * e.pageIndex;
    try {
      await this.fetchDocuments({
        idTerm: `${this.searchForm.value.keyword}*`,
        dept: this.searchForm.value.dept,
        typ: this.searchForm.value.typ,
        status: this.searchForm.value.status,
        limit: "" + limit,
        offset: "" + offset
      });
    } catch (err) {
      // this.appContextService.handleHTTPErrors(err, true, 'ERROR',)
    }
  }
  openModel(new_value: boolean, item: any = null) {
    if (item != null) {
      this.selected_application = cloneObject(item);
    }
    // console.log(new_value);
    if (this.modelv != new_value) {
      this.modelv = new_value;
      if (this.modelv) {
        this.modal_style = {
          visibility: "visible",
          opacity: 1
        };
      } else {
        this.modal_style = {
          visibility: "hidden",
          opacity: 0
        };
      }
    }
  }
  // async changeApplicationStatus() {
  //   this.appContextService.setProgressbarValue(true);
  //   this.processing = true;
  //   try {
  //     const updatedIem = await this.documentService.updatePhysician(
  //       { field: 'status', value: this.selected_application.status, name: '2222111' },
  //       this.selected_application.physician_id
  //     );
  //     this.fetchDocuments();
  //     this.appContextService.showSnackBarShow('Status Changed', 'SUCCESS');
  //   } catch (err) {
  //     console.log(err);
  //     this.appContextService.handleHTTPErrors({ error: err }, true, null, 'warn-panel');
  //   }
  //   this.processing = false;
  //   this.appContextService.setProgressbarValue(false);
  // }
}
