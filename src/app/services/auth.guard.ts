import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { AuthService, UserAuthState } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class UserAuthGuard {
  constructor(private router: Router, private authService: AuthService) {}

  canActivate(): Observable<boolean> | boolean {
    let x = new BehaviorSubject<boolean>(null);
    this.authService.authState.subscribe(v => {
      if (v != UserAuthState.default) {
        x.next(v == UserAuthState.userLoggedIn);
      }
    });

    return x.pipe(
      filter(v => v != null),
      map(v => {
        if (v == false) {
          //console.log('false trigger')
          this.router.navigate(['login']);
          return false;
        } else {
          //console.log('true');
          return true;
        }
      })
    );
  }
}
