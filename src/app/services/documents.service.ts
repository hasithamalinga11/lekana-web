import { DocumentListItem } from "../interfaces/document.interface";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AuthService } from "./auth.service";
import { DocSearch } from "../interfaces/docSearch.interface";
import { CreateDoc } from "../models/createDoc";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class DocumentService {
  // documents: DocumentListItem[] = [
  //     {
  //         name : 'Document',
  //         isApproved : true,
  //         desc : "This is a small description about my CV",
  //         type : 'One',
  //         department: 'Accounting',
  //         docTitle : 'AsithaBandara.pdf',
  //         author : 'Asitha',
  //     status
  //     },
  //     {
  //         name : 'Document',
  //         isApproved : true,
  //         desc : "This is a small description about my CV",
  //         type : 'One',
  //         department: 'Accounting',
  //         docTitle : 'AsithaBandara.pdf',
  //         author : 'Asitha'
  //     },        {
  //         name : 'Document',
  //         isApproved : true,
  //         desc : "This is a small description about my CV",
  //         type : 'One',
  //         department: 'Accounting',
  //         docTitle : 'AsithaBandara.pdf',
  //         author : 'Asitha'
  //     },        {
  //         name : 'Document',
  //         isApproved : true,
  //         desc : "This is a small description about my CV",
  //         type : 'One',
  //         department: 'Accounting',
  //         docTitle : 'AsithaBandara.pdf',
  //         author : 'Asitha'
  //     },        {
  //         name : 'Document 1',
  //         isApproved : false,
  //         desc : "This is a small description about my Poroject",
  //         type : 'One',
  //         department: 'HR department',
  //         docTitle : 'GayanProject.pdf',
  //         author : 'Gayan'
  //     }
  // ]

  constructor(private http: HttpClient, private authService: AuthService) {}

  fetchDocuments = async (opt: DocSearch): Promise<any> => {
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.authService.authToken.value
    });
    const res = this.http
      .post<any>(
        environment.apiOrigin + "/documents",
        {
          id: "" + Date.now(),
          execer: this.authService.userAccount.value.id,
          messageType: "search",
          offset: opt.offset || "0",
          limit: opt.limit || "15",
          idTerm: opt.idTerm || "*",
          nameTerm: "*",
          dept: opt.dept || "",
          typ: opt.typ || "",
          status: opt.status || "",
          tag: "",
          sort: "decending"
        },
        { headers: headers }
      )
      .toPromise();
    return res;
  };

  create = async (doc: CreateDoc) => {
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.authService.authToken.value
    });

    const res = this.http
      .post(environment.apiOrigin + "/documents", doc, { headers: headers })
      .toPromise();
    return res;
  };

  getPdf = async (id: string) => {
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.authService.authToken.value
    });

    const res = this.http
      .post(
        environment.apiOrigin + "/documents",
        {
          id: "" + Date.now(),
          execer: this.authService.userAccount.value.id,
          messageType: "get",
          documentId: id
        },
        { headers: headers }
      )
      .toPromise();
    return res;
  };
}
