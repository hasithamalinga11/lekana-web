import { NewUser } from '../dto/newUser.dto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { UserSearchOptions } from '../interfaces/userSearchOptions.interface';
import { CreateUser } from '../models/createUser';

@Injectable({providedIn:'root'})
export class UserPageService {
    // users:NewUser[] = [
    //     {
    //         username : 'John',
    //         email : 'john@abcd.com',
    //         phone : "0723456789",
    //         bio : "Hello. This is John Wick",
    //         avatar : "assets/img/profile_images/1.jpg"
    //     },
    //     {
    //         username : 'Mark',
    //         email : 'mark@abcd.com',
    //         phone : "0722346789",
    //         bio : "Hello. This is Mark",
    //         avatar : "assets/img/profile_images/2.jpg"
    //     }

    // ]
    constructor(private http: HttpClient, private authService : AuthService){}

    fetchUsers = async (options : UserSearchOptions):Promise<any> => {
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + this.authService.authToken.value
        });
        const response = await this.http.
            post<any>(
                environment.apiOrigin+'/accounts',
                {
                    id: "" + Date.now(),
                    execer: this.authService.userAccount.value.id,
                    messageType: "search",
                    offset: options.offset,
                    limit: options.limit,
                    idTerm: options.idTerm,
                    name: "",
                    email: "",
                    phone: ""
                }, {headers:headers})
                .toPromise();
        console.log('users',response)
        return response;
    }

    createUser = async (formData) => {

        let user : CreateUser = {
            id : ""+Date.now(),
            accountId : formData.email,
            messageType : "create",
            accountEmail : formData.email,
            execer : this.authService.userAccount.value.id,
            accountName : formData.username,
            accountPhone : formData.phone,
            accountPassword : "",
            accountCompany : formData.company,
            deviceToken : "",
            deviceType : "",
            accountRoles : formData.role
        }
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + this.authService.authToken.value
        });

        const response = this.http.
        post<any>(
            environment.apiOrigin+'/accounts',
            user, 
            {headers:headers})
            .toPromise();
        return response;
    }

    addRole = (formData) => {
        let addRole = {
            id: ""+Date.now(),
            execer: this.authService.userAccount.value.id,
            messageType:'addRole',
            accountId: formData.email,
            accountRole: formData.role
        }
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + this.authService.authToken.value
        });
        const response = this.http.
        post<any>(
            environment.apiOrigin+'/accounts',
            {addRole}, 
            {headers:headers})
            .toPromise();
        return response;
    }

    updateUser = (formData) => {
        let User = {
            id : ""+Date.now(),
            accountId : formData.email,
            messageType : "update",
            accountEmail : formData.email,
            execer : this.authService.userAccount.value.id,
            accountName : formData.username,
            accountPhone : formData.phone,
            accountPassword : "",
            accountCompany : formData.company,
            deviceToken : "",
            deviceType : ""
        }

        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + this.authService.authToken.value
        });

        const response = this.http.
        post<any>(
            environment.apiOrigin+'/accounts',
            User, 
            {headers:headers})
            .toPromise();
        return response;
        
    }
           
    deleteUser = (id:String) => {
        let user = {
            id: ""+ Date.now(),
            execer: this.authService.userAccount.value.id,
            messageType: "delete",
            accountId: id
        }
        const headers = new HttpHeaders({
            Authorization: 'Bearer ' + this.authService.authToken.value
        });

        const response = this.http.
        post<any>(
            environment.apiOrigin+'/accounts',
            {user}, 
            {headers:headers})
            .toPromise();
        return response;
    }
    // fetchUser = async (username:string) => {
    //     await setTimeout(() => {
    //         return this.users.find( user => user.username === username)
    //     }, 3000)
    // }

    // deleteUser = async (username:string) => {
    //     await setTimeout(() => {
    //         let user = this.users.find((user,i) => user.username === username);
    //         let index = this.users.indexOf(user);
    //         if(index > -1){
    //             this.users.splice(index, 1);
    //         }
    //     },3000)
    // }
}