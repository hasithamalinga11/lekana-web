import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "./auth.service";

export const enum UserAuthState {
  default = 0,
  userLoggedIn = 1,
  guest = 2
}
@Injectable({
  providedIn: "root"
})
export class AppCommonService {
  constructor(private authService: AuthService, private http: HttpClient) {}
  //examle post
  // savepost(post: any, postId: string = null, publishToAdmin: boolean = false): Observable<any> {
  //   const headers = new HttpHeaders({
  //     Authorization: 'Bearer ' + this.authService.authToken.value
  //   });
  //   return this.http.post(
  //     environment.apiOrigin + '/services/createPost?apiKey=' + environment.apiKey,
  //     { post, postId, publishToAdmin },
  //     {
  //       headers: headers
  //     }
  //   );
  // }
  //examle get
  // getMyPostById(postId: string): Observable<any> {
  //   const headers = new HttpHeaders({
  //     Authorization: 'Bearer ' + this.authService.authToken.value
  //   });
  //   const url = `${environment.apiOrigin}/services/post/${postId}?apiKey=${environment.apiKey}`;
  //   return this.http.get(url, {
  //     headers: headers
  //   });
  // }
}
