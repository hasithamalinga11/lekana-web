import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { getUUID } from '../utils/DateTimeUtils';
import { sha256 } from 'js-sha256';
import Base64 from 'crypto-js/enc-base64';

export class UserService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  registeUser(
    email: string,
    accountPassword: string,
    phone:string
  ): Promise<any> {
    return this.http
      .post(environment.apiOrigin + '/accounts', {
        id:""+Date.now(),
        messageType: 'register',
        execer: email,
        accountId: email,
        accountEmail: email,
        accountName: "",
        accountPhone: phone,
        accountPassword: sha256.hex(accountPassword),
        deviceToken: '',
        deviceType: ''
      })
      .toPromise();
  }

  loginUser(email: string, accountPassword: string): Promise<any> {
    // const httpOptions = {
    //   headers: new HttpHeaders({ 
    //     'Access-Control-Allow-Origin':'*',
    //     'Content-Type':'application/json'
    //   })
    // };

    return this.http
      .post(environment.apiOrigin + '/accounts', {
        id:"" + Date.now(),
        messageType: 'connect',
        execer: "admin@lekana.com",
        accountId: email,
        accountPassword: sha256.hex(accountPassword)
      })
      .toPromise();
  }

  async changePassword(oldp, newp): Promise<any> {
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.authService.authToken.value
    });
    const res = this.http
      .post(environment.apiOrigin + '/accounts', {
        id: ""+Date.now(),
        execer: this.authService.userAccount.value.id,
        messageType: "changePassword",
        accountId: this.authService.userAccount.value.id,
        oldPassword: sha256.hex(oldp),
        newPassword: sha256.hex(newp)
      }, {headers:headers}).toPromise();
    
    return res;
  }

  async createPhysician(data: any): Promise<any> {
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.authService.authToken.value
    });
    const results = await this.http
      .post<any>(environment.apiOrigin + '/physicians', data, { headers: headers })
      .toPromise();
    return true;
  }
  async updatePhysician(data: any, physician_id: string): Promise<any> {
    // const headers = new HttpHeaders({
    //   Authorization: 'Bearer ' + this.authService.authToken.value
    // });
    const headers = new HttpHeaders({
      Authorization: 'Basic QXBwQWRtaW4vT3JnMS9teWNoYW5uZWw6cGFzczA='
    });
    const results = await this.http
      .put<any>(environment.apiOrigin + '/physicians/' + physician_id, data, { headers: headers })
      .toPromise();
    return results;
  }

  async getPhysicians(limit: number): Promise<any> {
    // const headers = new HttpHeaders({
    //   Authorization: 'Bearer ' + this.authService.authToken.value
    // });
    const headers = new HttpHeaders({
      Authorization: 'Basic QXBwQWRtaW4vT3JnMS9teWNoYW5uZWw6cGFzczA='
    });
    let url = environment.apiOrigin + '/physicians?';
    url += `limit=${limit}`;
    const results = await this.http.get<any>(url, { headers: headers }).toPromise();
    return results;
  }
  async getPhysician(physician_id: string): Promise<any> {
    // const headers = new HttpHeaders({
    //   Authorization: 'Bearer ' + this.authService.authToken.value
    // });
    const headers = new HttpHeaders({
      Authorization: 'Basic QXBwQWRtaW4vT3JnMS9teWNoYW5uZWw6cGFzczA='
    });
    let url = environment.apiOrigin + '/physicians/' + physician_id;
    const results = await this.http.get<any>(url, { headers: headers }).toPromise();
    return results;
  }
}
