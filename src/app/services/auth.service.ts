import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { IUser } from "../models/IUser";
import { getUUID } from "../utils/DateTimeUtils";
import { Router } from "@angular/router";
import {
  localStorage_USER_Auth_token,
  localStorage_USER_Auth_user
} from "../constants/app.constants";

export const enum UserAuthState {
  default = 0,
  userLoggedIn = 1,
  guest = 2
}
@Injectable({
  providedIn: "root"
})
export class AuthService {
  authToken = new BehaviorSubject<string>(null);

  userAccount = new BehaviorSubject<IUser>(null);
  authState = new BehaviorSubject<number>(UserAuthState.default);

  constructor(private http: HttpClient, private router: Router) {}
  async saveUsersLocalStorage(user: IUser, token: string) {
    await localStorage.setItem(localStorage_USER_Auth_token, token);
    await localStorage.setItem(
      localStorage_USER_Auth_user,
      JSON.stringify(user)
    );
  }
  async authCheck(): Promise<boolean> {
    try {
      if (this.userAccount.value != null) {
        return true;
      } else {
        const storedToken = await localStorage.getItem(
          localStorage_USER_Auth_token
        );
        let storedUser: any = await localStorage.getItem(
          localStorage_USER_Auth_user
        );

        if (
          storedToken == "null" ||
          storedToken == null ||
          storedToken == undefined ||
          storedUser == null ||
          storedUser == undefined ||
          storedUser == "null"
        ) {
          return false;
        } else {
          storedUser = JSON.parse(storedUser);
          //console.log('Token', storedUser);
          this.authToken.next(storedToken);
          //const user = await this.accountFindByEmail(storedUser.email);
          await localStorage.setItem(
            localStorage_USER_Auth_user,
            JSON.stringify(storedUser)
          );
          if (storedUser == null) {
            return false;
          }
          this.userAccount.next(storedUser);
          this.authState.next(UserAuthState.userLoggedIn);
          return true;
        }
      }
    } catch (err) {
      throw err;
    }
  }
  async accountFindByEmail(email: string) {
    const headers = new HttpHeaders({
      Authorization: "Bearer " + this.authToken.value
    });

    const result = await this.http
      .post<any>(
        environment.apiOrigin + "/accounts",
        {
          uid: getUUID(),
          execer: email,
          messageType: "search",
          offset: 0,
          limit: 1,
          idTerm: "",
          name: "",
          email: email,
          phone: ""
        },
        { headers: headers }
      )
      .toPromise();

    if (result.accounts && result.accounts.length == 1) {
      return result.accounts[0];
    } else {
      throw "User not found";
    }
  }

  signout() {
    localStorage.setItem(localStorage_USER_Auth_token, null);
    localStorage.setItem(localStorage_USER_Auth_user, null);
    this.authState.next(UserAuthState.guest);
  }
}
