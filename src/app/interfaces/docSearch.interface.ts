export interface DocSearch {
    limit ?: string;
    offset ?: string;
    idTerm ?: string;
    status ?: string;
    typ ?: string;
    dept ?: string;
}