export interface UserSearchOptions {
    limit : number;
    offset : number;
    idTerm : string
}