export interface DocumentListItem {
  name: string;
  isApproved: boolean;
  desc: string;
  typ: string;
  department: string;
  docTitle: string;
  author: string;
  status: string;
  description: string;
  dept: string;
  creator: String;
  blobId: string;
}
