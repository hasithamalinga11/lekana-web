import { dictionaryNumberRandom } from './Utils';

declare var moment;

export const getCachedExpireTime = (): number => {
  return moment()
    .add(2, 'hours')
    .unix();
};

export const getCurrentDateTimeUnix = (): number => {
  const datetime = moment().unix();
  return datetime;
};

export const getRemainingTime = (unixstart: number): string => {
  //console.log(unixstart);
  return moment.unix(unixstart).from(moment(), true);
};

export const isCachedExpired = (expire_at: number): boolean => {
  const now_date = moment().unix();
  return expire_at < now_date;
};

export const getUUID = () => {
  return '' + moment().unix() + dictionaryNumberRandom(5);
};
