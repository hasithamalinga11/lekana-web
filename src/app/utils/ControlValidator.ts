import { AbstractControl, FormGroup } from '@angular/forms';

export class FormValidatorControls {
  meessages: any = {};
  constructor(private ValidateFields: string[]) {
    this.meessages = this.getValidationObject();
  }
  getValidationObject = (): any => {
    const reducer = (acc: any, currValue: any) => {
      acc['' + currValue] = null;
      return acc;
    };
    return this.ValidateFields.reduce(reducer, {});
  };
  findErrors(formGroup: FormGroup) {
    for (let item of this.ValidateFields) {
      const controlItem = formGroup.controls[item];
      const error_message = getFormValidationError(controlItem, item);
      this.meessages[item] = error_message;
    }
  }
}

export const getFormValidationError = (controlItem: AbstractControl, name: string): string => {
  const errors = controlItem.errors;
  const invalid = controlItem.invalid && (controlItem.dirty || controlItem.touched);
  let error_message = null;

  // first_name > convert to  >  First Name
  //name > Name

  let name_text = name
    .split('_')
    .map(text => {
      return text.length > 1 ? text[0].toUpperCase() + text.substr(1) : text.toUpperCase();
    })
    .join(' ');

  if (invalid) {
    if (errors.required) {
      error_message = name_text + ' is required.';
    } else if (errors.minlength) {
      error_message = name_text + ' must be at least ';
      error_message += errors.minlength.requiredLength;
      error_message += ' characters long.';
    } else if (errors.maxlength) {
      error_message = name_text + ' must have less than';
      error_message += errors.maxlength.requiredLength;
      error_message += ' characters long.';
    } else if (errors.min) {
      error_message = name_text + ' must be greater than ';
      error_message += errors.min.min;
      error_message += '.';
    } else if (errors.max) {
      error_message = name_text + ' must be less than ';
      error_message += errors.max.max;
      error_message += '.';
    } else if (errors.pattern) {
      error_message = name_text + ' not valid. ';
    } else if (errors.NotInList) {
      error_message = name_text + ' not valid. ';
    } else if (errors.email) {
      error_message = 'Email not valid ';
    }
  }
  return error_message;
};

export const MustMatch = (controlName: string, matchingControlName: string) => {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            return;
        }

        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}