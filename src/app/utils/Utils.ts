import { FormControl } from '@angular/forms';

declare var $: any;

export const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
export const Phone_REGEX = /^[0-9]*$/;
export const Number_REGEX = /^[0-9]*$/;
export const AlphaNumeric_REGEX = /^[a-z\d\-_\s]+$/i;
export const Alphabatic_REGEX = /^[a-zA-Z ]*$/;
export const Date_REGEX = /^(0[1-9]|[12][0-9]|3[01])[\- \/.](?:(0[1-9]|1[012])[\- \/.](19|20)[0-9]{2})$/;
export const Number_REGEX_withComma = /(\d{0,3},)?(\d{3},)?\d{0,3}/;
export const base64Encode = (str: string) => {
  return btoa(str);
};
export const base64Decode = (str: string) => {
  return atob(str);
};

export const validateIntegerNumber = (numberv: number): { error: string; valid: boolean } => {
  let err = '';
  let valid = true;
  if (numberv && !isNaN(numberv) && Number.isInteger(numberv)) {
  } else {
    valid = false;
    err = 'Number Not valid';
  }

  return { error: err, valid: valid };
};
export const validateEmail = (email: string): { error: string; valid: boolean } => {
  const parts1 = email.split('@');
  let err = '';
  let valid = true;
  if (parts1.length < 2) {
    valid = false;
    err = 'Email Not in valid format';
  } else if (parts1[1].split('.').length < 2) {
    valid = false;
    err = 'Email Not in valid format';
  }

  return { error: err, valid: valid };
};
export const number_ranges = [
  { divider: 1e18, suffix: 'E' },
  { divider: 1e15, suffix: 'P' },
  { divider: 1e12, suffix: 'T' },
  { divider: 1e9, suffix: 'G' },
  { divider: 1e6, suffix: 'M' },
  { divider: 1e3, suffix: 'k' }
];
export const getUrlParameter = (name: string, url: string) => {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(url);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
export const formatMatricNumber = (n): string => {
  for (var i = 0; i < number_ranges.length; i++) {
    if (n >= number_ranges[i].divider) {
      return (n / number_ranges[i].divider).toString() + number_ranges[i].suffix;
    }
  }
  return n.toString();
};

const possibleDictionary = 'ABCDEFGHKMNPQRSTUVWXYZabcdefghkmnpqrstuvwxyz23456789';

const possibleNumbers = '0123456789';
export const dictionaryNumberRandom = (len: number): number => {
  let text = '';

  for (var i = 0; i < len; i++)
    text += possibleNumbers.charAt(Math.floor(Math.random() * possibleNumbers.length));

  return parseInt(text);
};

export const dictionaryRandom = (len: number): string => {
  let text = '';

  for (var i = 0; i < len; i++)
    text += possibleDictionary.charAt(Math.floor(Math.random() * possibleDictionary.length));

  return text;
};

export const dictionaryTimeRandom = (len: number): string => {
  let text = '';

  for (var i = 0; i < len; i++)
    text += possibleDictionary.charAt(Math.floor(Math.random() * possibleDictionary.length));

  return text + Date.now();
};

export const RestObjectByKeysDefaults = (obj: any, keys: string[], defaultObj: any): any => {
  const d1 = cloneObject(defaultObj);
  keys.forEach((item: string) => {
    if (item.includes('.')) {
      const keys = item.split('.');
      obj[keys[0]][keys[1]] = d1[keys[0]][keys[1]];
    } else {
      obj[item] = d1[item];
    }
  });
  return obj;
};
export class JsUIUtils {
  constructor() {}
  public jsCollapse(query: string) {
    $('' + query).collapse('toggle');
  }
}
export const roundNumber = (v: number, delta: number): number => {
  return Math.round(v / delta) * delta;
};
export const convertToUFirstWordsStyle = (value: string): string => {
  return value == null
    ? ''
    : value
        .split(' ')
        .map(item => {
          item = item.toLowerCase();
          if (item.length > 1) {
            return item.charAt(0).toUpperCase() + item.slice(1);
          } else {
            return item.toUpperCase();
          }
        })
        .join(' ');
};
export const integerVld = (min: number) => {
  return (control: FormControl) => {
    const value = control.value;
    if (('' + value).length > 0) {
      if (isNaN(value)) {
        return { integerVld: true };
      }
      if (value < min) {
        return { integerVld: true };
      }
    }
    return null;
  };
};
export const tryValidateText = (value: string, validetors: any): boolean => {
  const fc = new FormControl(value, validetors);
  return fc.valid;
};
export const getNewUniqueTimeKey = (): string => {
  return 'Key_' + Date.now();
};
export const PositiveFloating = (min: number) => {
  return (control: FormControl) => {
    const value = control.value;

    if ((value + '').trim().length > 0) {
      if (value < 0) {
        return { positivefloating: true };
      } else if (('' + value).startsWith('-')) {
        return { positivefloating: true };
      } else {
        if (('' + value).length < 50) {
          if (('' + value).includes('-')) {
            return { positivefloating: true };
          } else {
            if (isNaN(value)) {
              return { positivefloating: true };
            }
          }
        } else {
          return { positivefloating: true };
        }
      }
      if (value < min) {
        return { positivefloating: true };
      }
    }
    return null;
  };
};

export const getInt = (vl: any): number => {
  const x = parseInt(vl, 10);
  return isNaN(x) ? 0 : x;
};
export const getDateString = (val: any): string => {
  const Date1 = new Date(val);
  const dd = Date1.getDate();
  const mm = Date1.getMonth() + 1; // January is 0!

  let dds = '' + dd;
  let mms = '' + mm;
  const yyyy = Date1.getFullYear();
  if (dd < 10) {
    dds = '0' + dd;
  }
  if (mm < 10) {
    mms = '0' + mm;
  }
  return yyyy + '-' + mms + '-' + dds;
};
export const get_uuid32 = () => {
  if (
    typeof window !== 'undefined' &&
    typeof window.crypto !== 'undefined' &&
    typeof window.crypto.getRandomValues !== 'undefined'
  ) {
    const buf = new Uint16Array(8);
    window.crypto.getRandomValues(buf);
    return (
      this.pad4(buf[0]) +
      this.pad4(buf[1]) +
      this.pad4(buf[2]) +
      this.pad4(buf[3]) +
      this.pad4(buf[4]) +
      this.pad4(buf[5]) +
      this.pad4(buf[6]) +
      this.pad4(buf[7])
    );
  } else {
    return (
      this.random4() +
      this.random4() +
      this.random4() +
      this.random4() +
      this.random4() +
      this.random4() +
      this.random4() +
      this.random4()
    );
  }
};
export const get_uuid8 = () => {
  if (
    typeof window !== 'undefined' &&
    typeof window.crypto !== 'undefined' &&
    typeof window.crypto.getRandomValues !== 'undefined'
  ) {
    const buf = new Uint16Array(4);
    window.crypto.getRandomValues(buf);
    return this.pad4(buf[0]) + this.pad4(buf[1]) + this.pad4(buf[2]) + this.pad4(buf[3]);
  } else {
    return this.random4() + this.random4() + this.random4() + this.random4();
  }
};
export const pad4 = num => {
  let ret = num.toString(16);
  while (ret.length < 4) {
    ret = '0' + ret;
  }
  return ret;
};
export const random4 = () => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
};

export const getIDFromURL = (url1: string) => {
  const index = (url1 + '').lastIndexOf('/');
  const id = (url1 + '').substring(index + 1, url1.length);
  return id;
};
export const parseLangCode = (langcode: string) => {
  return langcode == 'en' ? 0 : langcode == 'si' ? 1 : langcode == 'ta' ? 2 : -1;
};
export const shuffle = listData => {
  for (let i = listData.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [listData[i], listData[j]] = [listData[j], listData[i]];
  }
  return listData;
};

export const cloneObject = (data: any): any => {
  return JSON.parse(JSON.stringify(data));
};
export const cloneMap = (data: Map<any, any>): Map<any, any> => {
  const clonedMap = new Map<any, any>();
  data.forEach((v: any, k: any) => {
    clonedMap.set(k, cloneObject(v));
  });
  return clonedMap;
};
export const getKeys = (map: Map<string, any>): Array<string> => {
  const ar: Array<string> = [];
  map.forEach((v: any, k: string) => {
    ar.push(k);
  });
  return ar;
};
export const validateStringWithLenMatch = (str: string, min: number, max: number): boolean => {
  let v = false;
  if (str) {
    str = str.trim();
    if (str.length > min && str.length <= max) {
      v = true;
    }
  }
  return v;
};

export const listToGroup = (listdata: any[], removeEmpty: boolean = false): any => {
  const groups_: any = {};

  listdata.forEach(element => {
    if (element.cat == 1) {
      groups_[element.id] = { ...element, items: [] };
    }
  });

  listdata.forEach((element: any) => {
    if (element.cat == 0) {
      if (groups_[element.type].items) {
        groups_[element.type].items.push(element);
      } else {
        groups_[element.type].items = [];
        groups_[element.type].items.push(element);
      }
    }
  });
  let listdata1 = Object.values(groups_);
  if (removeEmpty) {
    listdata1 = listdata1.filter((value: any) => {
      return value.items.length > 0;
    });
  }

  return listdata1;
};
export const ObjectListToMap = (items: any[], keyProperty: string = 'id'): any => {
  const reducer = (acc: any, currValue: any) => {
    acc['' + currValue[keyProperty]] = currValue;
    return acc;
  };
  return items.reduce(reducer, {});
};
export const getNestedKey = (obj: any, key: string): any => {
  if (key.includes('.')) {
    const keys = key.split('.');
    return obj[keys[0]][keys[1]];
  } else {
    return obj[key];
  }
};

export const setNestedKey = (obj: any, key: string, v: any): any => {
  if (key.includes('.')) {
    const keys = key.split('.');
    obj[keys[0]][keys[1]] = v;
  } else {
    obj[key] = v;
  }
};

export const switchAllOrSome = (
  oldSearchQuery: any,
  searchQuery: any,
  tag: string,
  toggleValue: number = 0
) => {
  const v1 = getNestedKey(oldSearchQuery, tag).includes(toggleValue);
  if (v1 && getNestedKey(searchQuery, '' + tag).length > 1) {
    const newv = getNestedKey(searchQuery, '' + tag).filter(v => {
      return v > 0;
    });
    setNestedKey(searchQuery, tag, newv);
  } else if (!v1 && getNestedKey(searchQuery, '' + tag).includes(0)) {
    setNestedKey(searchQuery, tag, [0]);
  }
};
