import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSliderModule } from '@angular/material/slider';
import { MatSelectModule } from '@angular/material/select';
import {
  MatSnackBarModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatDialogModule,
  MatProgressBarModule,
  MatIconRegistry,
  MatTabsModule
} from '@angular/material';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatMenuModule } from '@angular/material/menu';
import { DateFormatPipe } from '../pipes/date-format.pipe';
import { PricePipe } from '../pipes/price.pipe';
import { StringTruncate } from '../pipes/stringsPipes';
@NgModule({
  declarations: [DateFormatPipe, PricePipe, StringTruncate],
  imports: [],
  exports: [DateFormatPipe, PricePipe, StringTruncate],
  providers: []
})
export class CommonUiModule {}
