export const environment = {
  production: false,
  apiOrigin: "http://137.117.37.159:7654/api",
  error_logging: true
};

// old - http://137.135.101.205:7654/api

// new - http://35.232.93.191:7654/api

// new-1 - http://35.193.233.111:7654/api
